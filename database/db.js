const mysql = require('mysql');

const con = mysql.createConnection({
  host: 'mysql',
  user: 'mysql',
  password: 'mysql',
  database: 'helios'
});

const db = {
  createTable: () => {
    con.query('CREATE TABLE IF NOT EXISTS opportunity(username VARCHAR(30));', (err, result) => {
      if (err) throw err;
      console.log('Table Created');
    });
  },

  connect: () => {
    con.connect(err => {
      if (err) {
        console.error('connection error', err.stack)
      } else {
        console.log('connected')
        module.exports.createTable();
      }
    });
  },

  insert: () => {
    const prefixName = "user-" + Math.random().toString();
    const text = 'INSERT INTO opportunity(username) VALUES(?)'
    const values = [`${prefixName}`]
    console.log(values);
    con.query(text, values, (err, result) => {
      if (err) throw err;
      console.log("Number of records inserted: " + result.affectedRows);
    });
  },

  update: () => {
    console.log('update');
  },

  delete: () => {
    console.log('delete')
  },
}

module.exports = db;
