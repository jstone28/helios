# helios

Testing Kafka with MySQL and Debezium

## Getting Started

Fron debezium's [docs](https://github.com/debezium/debezium-examples/tree/master/tutorial)

```bash
# Start the topology as defined in https://debezium.io/docs/tutorial/
docker-compose up

# Start MySQL connector
curl -i -X POST -H "Accept:application/json" -H  "Content-Type:application/json" http://localhost:8083/connectors/ -d @kafka/debezium-connect.json

# Consume messages from a Debezium topic
docker-compose exec kafka /kafka/bin/kafka-console-consumer.sh \
    --bootstrap-server kafka:9092 \
    --from-beginning \
    --property print.key=true \
    --topic dbserver1.inventory.customers

# Modify records in the database via MySQL client
docker exec -it mysql bash
mysql -u mysql -p
use inventory;
create table customers (id int);
insert into customers (id) values (1);

# Shut down the cluster
docker-compose down
```

## Resources

* https://github.com/debezium/debezium-examples/tree/master/tutorial
